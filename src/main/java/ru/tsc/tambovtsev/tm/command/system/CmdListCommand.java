package ru.tsc.tambovtsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.model.ICommand;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.util.Collection;

public final class CmdListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String DESCRIPTION = "Show commands list.";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand command : commands) {
            String name = command.getName();
            if (name != null && !name.isEmpty())
                System.out.println(command.getName());
        }
    }

}
