package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.api.service.IService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.IndexIncorrectException;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;
import ru.tsc.tambovtsev.tm.repository.AbstractRepository;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractEntity, R extends IRepository<M>> implements IService<M> {

    @Nullable
    protected final R repository;

    public AbstractService(@Nullable final R repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) throws AbstractException {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.add(model);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) throws AbstractException {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        repository.remove(model);
        return model;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return repository.findById(id);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

}
