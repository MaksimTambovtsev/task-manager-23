package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    @Nullable
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
